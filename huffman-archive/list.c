#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "heap.h"
#include "list.h"
/*Stana Iulian
  315CA*/

/*Listarea fisierelor (nume fisier + size).*/
void Listare_fisiere(List *lista, int16_t n)
{
    int16_t i;
    for (i = 0; i < n; i++ )
        printf("%-30s %d\n",lista[i].file_nume,lista[i].file_size);
}

/*Compararea a doua valori.(pentru qsort)*/
int compar (const void *a, const void *b)
{
	List *ia = (List *) a;
	List *ib = (List *) b;
	return strcmp(ia->file_nume, ib->file_nume);
}

