#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#include <unistd.h>
#include <sys/types.h>
#include "heap.h"
#include "list.h"
#include "compress.h"
#include "extract.h"
/*Stana Iulian
  315CA*/

/*257 de caractere maxim.*/
#define CHAR_SIZE 257


/*Folosesc variabile globale pentru rapiditate.*/
int nbits;
int32_t nbytes, checksum;
uint32_t file_size;
char *sir_caractere, curent_byte;


/*Compresie Huffman */
void huffman_compress(FILE *file,unsigned int frecv[], int nr_fis_val
                      , char **args, int fis_valide[])
{
    /*Aloc memorie.*/
    HuffmanNode     *NodHuff;
    ArchiveHeader   *Archiv  = malloc (sizeof(struct _ArchiveHeader));
    FileEntryHeader *FileEnt = calloc (1,sizeof(struct _FileEntryHeader));

    /*Vectorul de coduri.*/
    char *codes[CHAR_SIZE], code[CHAR_SIZE], *sir;
    int i, indice = 0;
    FILE *fis_cit;

    /*Arborele Huffman*/
    NodHuff = ArboreHuffman(frecv, &indice);

    /*Creez Headerul arhivei.*/
    Archiv->huffman_nodes_n   = indice;
    if(indice != 0)
        Archiv->huffman_root_node = indice - 1;
    else
        Archiv->huffman_root_node = 0;
    Archiv->file_entries_n    = nr_fis_val;

    /*Introduce Headerul si nodurile in fisier.*/
    fwrite(Archiv, sizeof(struct _ArchiveHeader), 1, file);
    fwrite(NodHuff, sizeof(struct _HuffmanNode), indice, file);

    /*Creez secventele pentru fiecare litera.*/
    if(indice != 0)
    Creez_secv(indice - 1, 0, NodHuff, code, codes);


    for(i = 0; i < nr_fis_val; i++)
    {
        fis_cit = fopen(args[fis_valide[i]],"rb");

        compress_file (fis_cit, codes);
        sir = Extrag_nume(args[fis_valide[i]]);
        strcpy(FileEnt->file_name, sir);
        FileEnt->bytes_n   = nbytes;
        FileEnt->checksum  = checksum;
        FileEnt->file_size = file_size;

        fwrite(FileEnt, sizeof(struct _FileEntryHeader), 1, file);
        if(nbytes != 0)
            fwrite(sir_caractere, sizeof(char), nbytes, file);
        free(sir);
        fclose(fis_cit);
    }


    /*Eliberez memoria ocupata de secvente.*/
    for(i = 0; i < CHAR_SIZE; i++)
        if(frecv[i] > 0)
        {
            free(codes[i]);
        }

    /*Eliberez memoria.*/
    free(sir_caractere);
    free(Archiv);
    free(NodHuff);
    free(FileEnt);
}



int main(int arg, char **args)
{

    int16_t i;
    int indice;

    if(strcmp(args[1],"compress") == 0)
    {
        FILE *file;
        unsigned char c;
        char *sir, **nume_fisiere = malloc (arg * sizeof(char *));


        /*Numarul de fisiere valide si vectorul lor.*/
        int nr_fis_val = 0, fis_valide[arg];

        /*Creez vectorul de frecvente si il initializez cu 0.*/
        unsigned int frecv[CHAR_SIZE];
        memset (frecv, 0, sizeof (frecv));

        /*Parcurge fisierele primite ca argument*/
        /*Depistarea fisierelor corupte si creerea vectorului de frecvente.*/
        for(indice = 3; indice < arg; indice++)
        {
            file = fopen(args[indice], "rb");
            if (!file)
            {
                fprintf(stderr,"Error: %s can't be opened\n",args[indice]);
            }
            else
            {
                sir = Extrag_nume(args[indice]);
                if(Verific_nume(sir, nume_fisiere, nr_fis_val) == 1)
                    {
                        free(sir);
                        fclose(file);
                        continue;
                    }
                nume_fisiere[nr_fis_val] = strdup(sir);

                fis_valide[nr_fis_val] = indice;
                nr_fis_val ++;
                while(fread(&c, sizeof(char), 1, file) == 1)
                {


                    frecv[(unsigned int)c]++;
                }
                fclose(file);
                free(sir);
            }
        }

        if(!nr_fis_val)
        {
            fprintf(stderr,"Error: No input files\n");
//            fclose(file);
            free(nume_fisiere);

            return 0;
        }

        file = fopen(args[2],"wb");

        huffman_compress(file, frecv, nr_fis_val, args, fis_valide);

        fclose(file);

        for(i = 0; i < nr_fis_val; i++)
            free(nume_fisiere[i]);
        free(nume_fisiere);

    }


    /*Extrag fisierele din arhiva.*/
    else if(strcmp(args[1],"extract") == 0)
    {
        char *Init = malloc (CHAR_SIZE * sizeof(char));
        char *path;
        FILE *fisier = fopen(args[2],"rb");

        getcwd(Init, CHAR_SIZE);


        /*Daca nu exista folder in care sa extrag.*/
        if(args[3] == NULL)
            H_Extract(fisier);

        else
            /*Schimb folderul.*/
            /*Daca folderul este dat din root.*/
            if(args[3][0] == '/' )
            {
                /*Verific corectitudinea folderului.*/
                /*Schimb directorul.*/
                if(chdir(args[3]) == -1)
                {
                    fprintf(stderr,"Error: Can't open %s\n",args[3]);
                    free(Init);
                    fclose(fisier);
                    return 0;
                }
                else if ((strcmp(args[3],"/") == 0))
                {
                    fprintf(stderr,"Error: Can't open %s\n",args[3]);
                    free(Init);
                    fclose(fisier);
                    return 0;
                }

                else
                {
                    H_Extract(fisier);
                    chdir(Init);
                }
            }

            /*Daca folderul se afla in folderul curent.*/
            else
            {
                path    = malloc((strlen(Init) + strlen(args[3]) + 2)
                                * sizeof(char));
                strcpy(path, Init);
                strcat(path, "/");
                strcat(path, args[3]);


                /*Verific corectitudinea folderului.*/
                /*Schimb directorul.*/
                if(chdir(args[3]) == -1)
                {
                    fprintf(stderr,"Error: Can't open %s\n",args[3]);
                    free(Init);
                    free(path);
                    fclose(fisier);
                    return 0;
                }
                else
                {
                    H_Extract(fisier);
                    chdir(Init);
                }

                free(path);
            }


        free(Init);
        fclose(fisier);
    }


    else if(strcmp(args[1],"list") == 0)
    {
        FILE *fisier = fopen(args[2],"rb");
        ArchiveHeader    Arhiva;
        HuffmanNode     *NoduriHuffman;
        FileEntryHeader  FileEntry;

        /*Citeste Headerul Arhivei*/
        fread(&Arhiva,sizeof(struct _ArchiveHeader),1,fisier);

        /*Aloc memorie pentru nodurile Huffman
          Citesc nodurile.*/
        NoduriHuffman = malloc (Arhiva.huffman_nodes_n * sizeof(struct _HuffmanNode));
        fread(NoduriHuffman,sizeof(struct _HuffmanNode),Arhiva.huffman_nodes_n,fisier);

        /*Aloc memorie pentru structura pe care o voi afisa.*/
        List *listare = malloc (Arhiva.file_entries_n * sizeof(struct Listare));

        /*Citesc fisierele de intrare.
          Sirul de simboluri comprimate.
          Le introduc intr-o structura (o sortez si o afisez).*/
        for (i = 0; i < Arhiva.file_entries_n; i++)
        {
            char *charac;
            fread(&FileEntry,sizeof(struct _FileEntryHeader),1,fisier);

            strcpy(listare[i].file_nume, FileEntry.file_name);
            listare[i].file_size = FileEntry.file_size;


            charac = malloc (FileEntry.bytes_n * sizeof(char));
            fread(charac,sizeof(char),FileEntry.bytes_n,fisier);
            free(charac);

        }

        qsort(listare, Arhiva.file_entries_n, sizeof(struct Listare), compar);
        Listare_fisiere (listare, Arhiva.file_entries_n);

        /*Eliberez memoria*/
        free(NoduriHuffman);
        free(listare);
        fclose(fisier);
        return 0;

    }

    return 0;
}
