#ifndef _Extract_H
#define _Extract_H

/*Decodific un caracter si formez un sir de 8 caractere.*/
char *Decodificare(char);

/*Extragere de tip Huffman.*/
void Extract(FileEntryHeader *, FILE *, HuffmanNode *, int);

/*Functia de Extract.*/
void H_Extract(FILE *);

#endif
