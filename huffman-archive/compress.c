#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "heap.h"
#include "compress.h"

/*Stana Iulian
  315CA*/


/*257 de caractere maxim.*/
#define CHAR_SIZE 257

/*Folosesc variabile globale pentru rapiditate.*/
int nbits;//, current_byte;
int32_t nbytes, file_size;
uint32_t checksum;
char *sir_caractere, curent_byte;

/*Verific numele fisierului, si aparitia acestora.*/
int Verific_nume(char *sir, char **nume_fisiere, int indice)
{
    int i;
    for(i = 0; i < indice; i++)
        if(strcmp(sir, nume_fisiere[i]) == 0)
            return 1;

    return 0;
}

/*Extrag numele fisierului din mai multe cai.*/
char *Extrag_nume(char *args)
{
    char *sir = malloc(30);
    unsigned int i, j;
    for(i = strlen(args); i > 0; i--)
        if(args[i] == '/')
            break;
    if (i != 0 || (args[i] == '/')) i++;
    for(j = i; j < strlen(args); j++)
        sir[j-i] = args[j];
    sir[j-i] = '\0';
    return sir;
}

/*Creez seventele asociate fiecarui caracter in parte.*/
void Creez_secv(int root, int level, HuffmanNode *NodHuff,
           char codul_format[], char *codes[])
{

  if ((NodHuff[root].left == -1) && (NodHuff[root].right == -1))
    {

        /*Pun terminatorul de sir.*/
        codul_format[level] = '\0';
        /*Copiez in memorie codul obtinut.*/
        codes[ (int) NodHuff[root].value] = strdup (codul_format);
    }
  else
    {

    /*Merg pe fiul stang al arborelui si adaug 0.*/
    if (NodHuff[root].left != -1)
    {
        codul_format[level] = '0';
        Creez_secv(NodHuff[root].left, level + 1, NodHuff, codul_format, codes);
    }

    /*Merg pe fiul drept al arborelui si adaug 1.*/
    if (NodHuff[root].right != -1)
    {
        codul_format[level] = '1';
        Creez_secv(NodHuff[root].right, level + 1, NodHuff, codul_format, codes);
    }
    }
}

/*Introduc cate un caracter.(din sirul de caractere asociate)*/
void putbit(char caracter)
{

    /*Shiftez cu cate o pozitie, bitul curent.*/
    curent_byte <<=1;

    /*In cazul in care caracterul este '1' il pun in byte.*/
    /*Daca nu este '1' nu fac nimica deoarece shiftarea a adaugat '0'.*/
    if (caracter == '1')
        curent_byte |= 1;

    /*Inca un bit.*/
    nbits++;

    /*Daca s-a ajuns la un bit complet il bag in sir.*/
    if (nbits == 8)
    {
        nbytes++;
        sir_caractere = realloc (sir_caractere, nbytes * sizeof(char));
        sir_caractere[nbytes - 1] = curent_byte;
        nbits       = 0;
        curent_byte = 0;
    }
}

void compress_file (FILE * fis_cit, char *codes[])
{
    unsigned char caracter;
    char *sir;

    /*Initializez variabilele globale.*/
    curent_byte = 0;
    nbits       = 0;
    nbytes      = 0;
    checksum    = 0;
    file_size   = 0;

    /*Extrag un caracter.*/
    while(fread(&caracter,sizeof(char),1,fis_cit))
    {
        /*Maresc marimea fisierului si checksum.*/
        file_size ++;
        checksum += caracter;

        /*Pun codul in sir_caractere.*/
        for (sir = codes[caracter]; *sir; sir++)
            putbit(*sir);

    }

    /*completez ultimul bit*/
    while (nbits)
        putbit('0');
}

/*Creez arborele Huffman.*/
HuffmanNode * ArboreHuffman(unsigned int frecv[], int *indice)
{

    HuffmanNode     *NodHuff = malloc (sizeof(struct _HuffmanNode));
    HeapVector      *Heap    = CreazaVector(CHAR_SIZE);


    /*Elementele din Heap.*/
    elem Valoare, ValA, ValB, ValC;
    int i;

    /*Introduc in heap caracterele gasite.(dupa frecventa si indice)*/
    for(i = 0; i < CHAR_SIZE; i++)
        if(frecv[i] > 0)
        {
            NodHuff = realloc(NodHuff,((*indice) + 1)*sizeof(struct _HuffmanNode));
            NodHuff[*indice].value = (char) i;
            NodHuff[*indice].right = -1;
            NodHuff[*indice].left  = -1;
            Valoare.indice    = (*indice);
            Valoare.frecventa = frecv[i];
            (*indice) ++;
            AdaugaValoare(Heap, Valoare);
        }

    if(Heap->dimVect == 1)
    {
        NodHuff = realloc(NodHuff,((*indice) + 1)*sizeof(struct _HuffmanNode));
        NodHuff[*indice].value = '\0';
        NodHuff[*indice].right = -1;
        NodHuff[*indice].left  = 0;
        (*indice) ++;

    }

    /*Creez arborele Huffman.*/
    while(Heap->dimVect > 1)
    {
        ValA = ExtrageMinim(Heap);
        ValB = ExtrageMinim(Heap);
        NodHuff = realloc(NodHuff,((*indice) + 1)*sizeof(struct _HuffmanNode));
        NodHuff[*indice].value = '\0';
        NodHuff[*indice].right = ValA.indice;
        NodHuff[*indice].left  = ValB.indice;
        ValC.frecventa = ValA.frecventa + ValB.frecventa;
        ValC.indice    = (*indice);
        AdaugaValoare(Heap, ValC);

        (*indice) ++;
    }
    ElibereazaVector(Heap);


    return NodHuff;


}

