#ifndef _Heap_H
#define _Heap_H
#define MAX_FILENAME_SIZE 30

/* Stana Iulian
    315CA */


/*Structura elementelor din Heap.*/
typedef struct Element
{
    /*Frecventa = nr de aparitii in text.
      Indicele  = indicile Nodului Huffman.*/
    int indice, frecventa;
} elem;


/*Structura vectorului de heapuri.*/
typedef struct heap_vector_struct
{
    /*Vectorul de heapuri.*/
    struct Element *values;
    /*Dimensiunea actuala a Vectorului.*/
    int dimVect;
    /*Capacitatea maxima.*/
    int capVect;

} HeapVector;


/*Structura Hederului arhivei.*/
typedef struct _ArchiveHeader {
    int16_t huffman_nodes_n;
    uint16_t huffman_root_node;
    int16_t file_entries_n;
} __attribute__((__packed__)) ArchiveHeader;


/*Structura Fisierelor de intrare.*/
typedef struct _FileEntryHeader {
    char file_name[MAX_FILENAME_SIZE];
    int32_t file_size;
    int32_t bytes_n;
    uint32_t checksum;
} __attribute__((__packed__)) FileEntryHeader;


/*Structura nodului Huffman.*/
typedef struct _HuffmanNode {
    unsigned char value;
    int16_t left, right;
} __attribute__((__packed__)) HuffmanNode;


/*Crearea unui Vector de Heap.(dimensiune maxima)*/
HeapVector* CreazaVector(int);

/*Eliberearea memoriei Heapului.*/
void ElibereazaVector(HeapVector *);

/*Parintele elementului din pozitia poz.*/
int Parinte(HeapVector *, int);

/*Fiul stanga al elementului din pozitia poz.*/
int DescS(HeapVector *, int);

/*Fiul dreapta al elementului din pozitia poz.*/
int DescD(HeapVector *, int);

/*Coborarea in Heap a unui element dintr-o pozitie poz.*/
void CoboaraValoare(HeapVector *, int);

/*Urcarea in Heap a unui element dintr-o pozitie poz.*/
void UrcaValoare(HeapVector *, int);

/*Returneaza elementul minim din Heap.*/
elem Minim(HeapVector *);

/*Extrag elementul minim din Heap si il si sterg.*/
elem ExtrageMinim(HeapVector *);

/*Adauga un element in Heap.*/
void AdaugaValoare(HeapVector *, elem );

/*Printeaza Heap-ul.*/
void PrintHeap(HeapVector *);


#endif
