#ifndef _Compress_H
#define _Compress_H

/*Verific numele fisierului, si aparitia acestora.*/
int Verific_nume(char *, char **, int);

/*Extrag numele fisierului din mai multe cai.*/
char *Extrag_nume(char *);

/*Creez seventele asociate fiecarui caracter in parte.*/
void Creez_secv(int , int , HuffmanNode *, char [], char *[]);

/*Introduc cate un caracter.(din sirul de caractere asociate)*/
void putbit(char);

/*Citesc din fisier caractere si le codez.*/
void compress_file (FILE *, char *[]);

/*Creez arborele Huffman.*/
HuffmanNode * ArboreHuffman(unsigned int [], int *);

#endif
