#ifndef _List_H
#define _List_H

/*Structura pentru Sortarea fisierelor de Listare.*/
typedef struct Listare
{
    char file_nume[30];
    int32_t file_size;
} List;

/*Listarea fisierelor (nume fisier + size).*/
void Listare_fisiere(List *, int16_t);

/*Compararea a doua valori.(pentru qsort)*/
int compar (const void *, const void *);


#endif
