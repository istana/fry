#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include "heap.h"
/*Iulian Stana
  315CA*/

/*Crearea unui Vector de Heap.(dimensiune maxima)*/
HeapVector* CreazaVector(int capVect)
{
    HeapVector *heap = malloc (sizeof(struct heap_vector_struct));
    heap->values  = malloc (capVect * sizeof(struct Element));
    heap->dimVect = 0;
    heap->capVect = capVect;

    return heap;

}
/*Eliberearea memoriei Heapului.*/
void ElibereazaVector(HeapVector *h)
{
    free(h->values);
    free(h);
}

/*Parintele elementului din pozitia poz.*/
int Parinte(HeapVector *h, int poz)
{
    if(poz == 0 || poz > h->dimVect)
            return -1;
    else return (poz - 1)/2;
}

/*Fiul stanga al elementului din pozitia poz.*/
int DescS(HeapVector *h, int poz)
{
    if((2*poz + 1) >= h->dimVect)
            return -1;
    else return 2*poz + 1;
}

/*Fiul dreapta al elementului din pozitia poz.*/
int DescD(HeapVector *h, int poz)
{
    if((2*poz + 2) >= h->dimVect)
            return -1;
    else return 2*poz + 2;
}

/*Coborarea in Heap a unui element dintr-o pozitie poz.*/
void CoboaraValoare(HeapVector *h, int poz)
{
    int s,d,sw=1,swap=-1;
    elem aux;
    s = DescS(h,poz);
    d = DescD(h,poz);
    while(sw)
    {
    if ((s!= -1) && (d == -1))
            swap = s;
    else if((s!= -1) && h->values[s].frecventa <= h->values[d].frecventa)
            swap = s;
    else if((d!= -1) && h->values[s].frecventa <= h->values[d].frecventa)
            swap = d;
    else if((s!= -1) && h->values[s].frecventa >= h->values[d].frecventa)
            swap = d;
    else if((d!= -1) && h->values[s].frecventa >= h->values[d].frecventa)
            swap = s;

    if((swap!= -1) && h->values[swap].frecventa <= h->values[poz].frecventa )
        {
            aux = h->values[swap];
            h->values[swap] = h->values[poz];
            h->values[poz] = aux;
            poz = swap;
            s = DescS(h,poz);
            d = DescD(h,poz);
            swap = -1;
        }
    else {sw = 0;}


    }
}

/*Urcarea in Heap a unui element dintr-o pozitie poz.*/
void UrcaValoare(HeapVector *h, int poz)
{
    int p, sw = 1;
    elem aux;
    p = Parinte(h, poz);
    while(sw)
    {
    if ((p != -1) && h->values[p].frecventa >= h->values[poz].frecventa)
    {
        aux = h->values[p];
        h->values[p] = h->values[poz];
        h->values[poz] = aux;
        poz = p;
        p = Parinte(h, poz);
    }
    else {sw = 0;}
    }
}

/*Returneaza elementul minim din Heap.*/
elem Minim(HeapVector *h)
{
    return h->values[0];
}

/*Extrag elementul minim din Heap si il si sterg.*/
elem ExtrageMinim(HeapVector *h)
{
    elem a = h->values[0];
    h->values[0] = h->values[h->dimVect-1];
    h->dimVect--;
    CoboaraValoare(h,0);
    return a;

}

/*Adauga un element in Heap.*/
void AdaugaValoare(HeapVector *h, elem val)
{
    if(h->dimVect + 1 == h->capVect)
    {
        h->capVect++;
        h->values = realloc(h->values, h->capVect * sizeof(struct Element));
    }
    h->values[h->dimVect] = val;
    h->dimVect ++;
    UrcaValoare(h, h->dimVect-1);
}

/*Printeaza Heap-ul.(doar frecventele)*/
void PrintHeap(HeapVector *heap)
{
    int i;
    for(i = 0; i < heap->dimVect; i++)
        printf("%d  ",heap->values[i].frecventa);
    printf("\n");
}
