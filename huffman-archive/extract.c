#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "heap.h"


/*Decodific un caracter si formez un sir de 8 caractere.*/
char *Decodificare(char car)
{
    char *cod = malloc(8);
    int i;

    for(i = 0; i < 8; i++)
    {

        if (!(car & 1))
            cod[i] = '0';
        else
            cod[i] = '1';

        car >>=1;
    }

    return cod;
}


/*Extragere de tip Huffman.*/
void Extract(FileEntryHeader *FileEntry, FILE * fisier,
             HuffmanNode *NodHuff, int root_arb)
{
    char *sir_carac, *sir_out, *cod;
    int32_t biti  = 0;
    uint32_t check = 0;
    int32_t file_s = 0;
    int contor = 1;

    /*Initializari.*/
    FILE *fis_out;
    int nrbiti = 0, root;
    char car;

    root = root_arb;

    /*Aloc memorie si citesc blocul din fisier.*/
    sir_carac = malloc (FileEntry->bytes_n * sizeof(char));
    sir_out   = malloc (FileEntry->file_size * sizeof(char));

    fread(sir_carac,sizeof(char),FileEntry->bytes_n,fisier);


    /*Decodificarea.*/
    while(biti < FileEntry->bytes_n && file_s < FileEntry->file_size)
    {
        /*In cazul in care blocul de 8 biti este ocupat mai citesc un char*/
        if(nrbiti == 8)
            {
                nrbiti = 0;
                free(cod);
            }

        if(nrbiti == 0)
            {
                car = sir_carac[biti];
                cod = Decodificare(car);
            }

        /*Parcurg arborele.*/
        while(((NodHuff[root].right != -1) || (NodHuff[root].left != -1))
               && (nrbiti < 8))
        {
            if(cod[7-nrbiti] == '0')
                {
                    root = NodHuff[root].left;
                }
            else
                {
                    root = NodHuff[root].right;
                }

            nrbiti ++;
        }

        /*Daca este frunza il bag in sir.*/
        if((NodHuff[root].right == -1) && (NodHuff[root].left == -1))
        {
            check += NodHuff[root].value;
            sir_out = realloc (sir_out, (file_s + 1) * sizeof(char));
            sir_out[file_s] = NodHuff[root].value;
            file_s ++;
            root = root_arb;
        }

        /*In cazul in care sunt 8 biti adug la unarul de bytes 1.*/
        if(nrbiti == 8)
            {
                biti++;
            }

    }

    int verifica = 0, i;

    /*Verific ultimul bloc.*/
    if(nrbiti < 8)
    {
        for(i = nrbiti; i < 8 ; i++)
            if(cod[7 - i] == '1')
            {
                verifica = 1;
            }
    }


    if (nrbiti < 8)
        free(cod);

    /*Daca ultimul bloc este completat cu zero.*/
    if (verifica == 0 && nrbiti!=8)
        biti++;

    if(check != FileEntry->checksum || biti != FileEntry->bytes_n
            || file_s != FileEntry->file_size)
    {
        fprintf(stderr,"Error: %s is CORRUPT\n", FileEntry->file_name);
        contor = 0;
    }

    /*Daca totul a mers perfect creez fisierul.*/
    if(contor)
    {
        fis_out = fopen(FileEntry->file_name, "wb");
        fwrite(sir_out, sizeof(char), FileEntry->file_size, fis_out);
        fclose(fis_out);
    }

    free(sir_out);
    free(sir_carac);

}


/*Functia de Extract.*/
void H_Extract(FILE *fisier)
{

    FILE *fis_out;
    ArchiveHeader    *Arhiva        = calloc (1,sizeof(struct _ArchiveHeader));
    FileEntryHeader  *FileEntry     = calloc (1,sizeof(struct _FileEntryHeader));
    HuffmanNode      *NodHuff;

    int i;

    /*Citeste Headerul Arhivei*/
    fread(Arhiva,sizeof(struct _ArchiveHeader),1,fisier);

    /*Aloc memorie pentru nodurile Huffman
      Citesc nodurile.*/
    NodHuff = malloc (Arhiva->huffman_nodes_n * sizeof(struct _HuffmanNode));
    fread(NodHuff,sizeof(struct _HuffmanNode),Arhiva->huffman_nodes_n,fisier);


    /*Citesc fisierele de intrare.
      Sirul de simboluri comprimate.*/
    for (i = 0; i < Arhiva->file_entries_n; i++)
    {
        fread(FileEntry,sizeof(struct _FileEntryHeader),1,fisier);
        /*Decodificare si extragere.*/
        if(FileEntry->file_size > 0)
        {
            Extract(FileEntry, fisier, NodHuff, Arhiva->huffman_root_node);
        }

        /*Caz separat Fisierul cu nici un simbol.*/
        else
            if(FileEntry->file_size < 0)
                fprintf(stderr,"Error: %s is CORRUPT\n", FileEntry->file_name);
        else
            if(FileEntry->checksum == 0 && FileEntry->bytes_n == 0)
            {
                fis_out = fopen(FileEntry->file_name,"wb");
                fclose(fis_out);
            }
    }
    /*Eliberez memoria*/
    free(NodHuff);
    free(Arhiva);
    free(FileEntry);


}

