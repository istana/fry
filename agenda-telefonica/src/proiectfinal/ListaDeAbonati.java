/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package proiectfinal;

import java.io.*;
import java.util.regex.Pattern;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author iulian
 */
public class ListaDeAbonati extends AbstractTableModel{


    public CarteDeTelefon carte = new CarteDeTelefon();

     /**
     * @return   none.
     * Constructorul clasei.
     */
    public ListaDeAbonati() {
    }



     /**
     * @param    rowIndex.
     * @return   Object.
     * Sterge si returneaza un anumit abonat.
     */
    public Object getRow(int rowIndex){
        return carte.removeAbonat(rowIndex);
    }//End getRow.
    
    /**
     * @param    none.
     * @return   CarteDeTelefon.
     * Returneaza cartea de telefon.
     */
    public CarteDeTelefon getCarte() {
        return carte;
    }

    /**
     * @param    CarteDeTelefon.
     * @return   none.
     * Seteaza CarteDeTelefon.
     */
    public void setCarte(CarteDeTelefon carte) {
        this.carte = carte;
    }

    /**
     * @param    String.
     * @return   none.
     * Lucru cu sistemul de operare Open/Save.
     */
    public void openCarte(String fisier){

		FileInputStream fis = null;
		ObjectInputStream in = null;
		try {
			fis = new FileInputStream(fisier);
			in = new ObjectInputStream(fis);
			this.setCarte((CarteDeTelefon) in.readObject());
			in.close();
                        this.fireTableDataChanged();
                } catch (IOException ex) {
                        System.out.println("Error!");
		} catch (ClassNotFoundException ex) {
                        System.out.println("Error!");
		}
    }

    public void saveCarte(String fisier){
                ObjectOutputStream out = null;
        FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(fisier);
			out = new ObjectOutputStream(fos);
			out.writeObject(this.getCarte());
			out.close();
		} catch (IOException ex) {
			System.out.println("Error!");
		}

    }

    public int getRowCount() {
        return carte.NrDeAbonati();
    }

    public int getColumnCount() {
        return 4;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
       switch(columnIndex){
           case (0): return carte.getAbonat(rowIndex).getNume();
           case (1): return carte.getAbonat(rowIndex).getPrenume();
           case (2): return carte.getAbonat(rowIndex).getCNP();
           case (3): return carte.getAbonat(rowIndex).getNrTel();

           default: return "Error";
       }

    }

    @Override
    public String getColumnName(int column) {

        String name[] = {"Nume","Prenume","CNP","Telefon"};
        return name[column];
    }

    /**
     * @param    Abonat.
     * @return   none.
     * Adaugarea unui abonat in tabela.
     */
    public void addAbonat(Abonat a){
        carte.addAbonat(a);
        this.fireTableDataChanged();
    }//End addAbonat

     /**
     * @param    String.
     * @return   none.
     * Sorteaza dupa un anumit criteriu.
     */
    public void sortare(String nume){

        if("Nume".equals(nume))
           carte.sortareNume();
        else if("Prenume".equals(nume))
           carte.sortarePrenume();
        else if("CNP".equals(nume))
           carte.sortareCNP();
        else if("Numar Telefon".equals(nume))
           carte.sortareNrTel();
        
        this.fireTableDataChanged();

    }//End sortare.

    /**
     * @param nume
     * @param carte
     * Metode de cautare.
     */
    private void cautaNume(String nume, CarteDeTelefon carte){

        CarteDeTelefon nouaCarte = new CarteDeTelefon();
        for (int i = 0; i < carte.NrDeAbonati(); i++) {
            Abonat a = carte.getAbonat(i);

            String rgx = nume + "\\w*";
            if(Pattern.matches(rgx, a.getNume())){
                nouaCarte.addAbonat(a);
            }
 
        }

        this.carte = nouaCarte;
    }

    private void cautaPrenume(String nume, CarteDeTelefon carte){

        CarteDeTelefon nouaCarte = new CarteDeTelefon();
        for (int i = 0; i < carte.NrDeAbonati(); i++) {
            Abonat a = carte.getAbonat(i);

            String rgx = nume + "\\w*";
            if(Pattern.matches(rgx, a.getPrenume())){
                nouaCarte.addAbonat(a);
            }

        }

        this.carte = nouaCarte;
    }

     private void cautaCNP(String nume, CarteDeTelefon carte){

        CarteDeTelefon nouaCarte = new CarteDeTelefon();
        for (int i = 0; i < carte.NrDeAbonati(); i++) {
            Abonat a = carte.getAbonat(i);

            String rgx = nume + "\\w*";
            if(Pattern.matches(rgx, a.getCNP())){
                nouaCarte.addAbonat(a);
            }
 
        }

        this.carte = nouaCarte;
    }

    private void cautaNrTel(String nume, CarteDeTelefon carte){

        CarteDeTelefon nouaCarte = new CarteDeTelefon();
        for (int i = 0; i < carte.NrDeAbonati(); i++) {
            Abonat a = carte.getAbonat(i);

            String rgx = nume + "\\d*";
            if(Pattern.matches(rgx, a.getNrTel())){
                nouaCarte.addAbonat(a);
            }
 
        }

        this.carte = nouaCarte;
    }

    public void cauta(String nume, String cauta, CarteDeTelefon carte){

        if("Nume".equals(nume))
           cautaNume(cauta, carte);
        else if("Prenume".equals(nume))
           cautaPrenume(cauta, carte);
        else if("CNP".equals(nume))
           cautaCNP(cauta, carte);
        else if("Numar Telefon".equals(nume))
           cautaNrTel(cauta, carte);

        this.fireTableDataChanged();

    }//End sortare.

}
