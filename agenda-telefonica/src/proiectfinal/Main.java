/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package proiectfinal;

import java.util.*;


/**
 *
 * @author iulian
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        final GUI g = new GUI();
        final Intro i = new Intro();

        i.setVisible(true);
        Timer t=new Timer();
        t.schedule(new TimerTask(){
            public void run(){
                i.setVisible(false);
                g.setVisible(true);
            }
       },3000);
    }
}
