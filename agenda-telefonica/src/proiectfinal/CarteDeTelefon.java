/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package proiectfinal;



import java.io.Serializable;
import java.util.*;


/**
 *
 * @author iulian
 */
public class CarteDeTelefon implements Serializable{
    
    List <Abonat> carte = new ArrayList<Abonat>();
    
    /**
     * @param    none.
     * @return   Abonat.
     * Constructor care primeste ca argumet un abonat, pe care il introduce
     * in lista.
     */
    public CarteDeTelefon(Abonat a) {
        this.carte.add(a);

    }//End CarteDeTelefon.

    /**
     * @param    none.
     * @return   none.
     * Constructor vid pentru Tabela initaiala.
     */
    public CarteDeTelefon() {
    }//End CarteDeTelefon.

    /**
     * @param    none.
     * @return   int.
     * Returneaza numarul de abonati.
     */
    public int NrDeAbonati(){
        return this.carte.size();
    }//End NrDeAbonati.

     /**
     * @param    Abonat.
     * @return   none.
     * Adauga la lista un abonat.
     */
    public void addAbonat(Abonat a) {
        this.carte.add(a);
    }//End addAbonat.

     /**
     * @param    Abonat.
     * @return   none.
     * Adauga la lista un abonat.
     */
    public Abonat removeAbonat(int index) {
        return this.carte.remove(index);
    }//End addAbonat.

    /**
     * @param    int.
     * @return   Abonat.
     * Returneaza un abonat de pe o anumita pozitie.
     */
    public Abonat getAbonat(int index){
        return carte.get(index);
    }//End getAbonat.

    /**
     * @param    none.
     * @return   none.
     * Sorteaza dupa Nume.
     */
    public void sortareNume(){

        Comparator comparator;
        comparator = new Comparator(){

            public int compare(Object o1, Object o2) {

                String nume1 = ((Abonat) o1).getNume();
                String nume2 = ((Abonat) o2).getNume();
                    return nume1.compareTo(nume2);

            }

        };//end Functie de comparare anonima.


        Collections.sort(carte,comparator);


    }//End sorateNume.

    /**
     * @param    none.
     * @return   none.
     * Sorteaza dupa Prenume.
     */
    public void sortarePrenume(){

        Comparator comparator;
        comparator = new Comparator(){

            public int compare(Object o1, Object o2) {

                String prenume1 = ((Abonat) o1).getPrenume();
                String prenume2 = ((Abonat) o2).getPrenume();
                    return prenume1.compareTo(prenume2);

            }

        };//end Functie de comparare anonima.

        Collections.sort(carte,comparator);

    }//End soratePrenume.

    /**
     * @param    none.
     * @return   none.
     * Sorteaza dupa CNP.
     */
    public void sortareCNP(){

        Comparator comparator;
        comparator = new Comparator(){

            public int compare(Object o1, Object o2) {

                Long CNP1 = Long.getLong(((Abonat) o1).getCNP());
                Long CNP2 = Long.getLong(((Abonat) o2).getCNP());
                if(CNP1 > CNP2)
                    return 1;
                else if(CNP1 == CNP2)
                    return 0;
                else return -1;
            }

        };//end Functie de comparare anonima.

        
        Collections.sort(carte,comparator);


    }//End sortareCNP.

    /**
     * @param    none.
     * @return   none.
     * Sorteaza dupa Numarul de telefon.
     */
    public void sortareNrTel(){

        Comparator comparator;
        comparator = new Comparator(){

            public int compare(Object o1, Object o2) {

                Long NrTelefon  = Long.getLong(((Abonat) o1).getNrTel());
                Long NrTelefon1 = Long.getLong(((Abonat) o2).getNrTel());
                if(NrTelefon > NrTelefon1)
                    return 1;
                else if(NrTelefon == NrTelefon1)
                    return 0;
                else return -1;
            }

        };//end Functie de comparare anonima.


        Collections.sort(carte,comparator);


    }//End sortareNrTel.



    @Override
    public String toString() {

        return carte.toString();

    }//End toString.



}
