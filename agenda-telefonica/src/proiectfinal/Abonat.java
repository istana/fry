/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package proiectfinal;


import java.io.Serializable;
import java.util.regex.Pattern;

/**
 *
 * @author iulian
 */
public class Abonat extends Throwable implements Serializable{

    private String nume, prenume;
    private String CNP;
    private String NrTel;

    public Abonat(String nume, String prenume, String CNP, String NrTel)
                                        throws Exception {

        if (Pattern.matches("\\w*", nume))
                this.nume = nume;
            else throw new Exception();

        if (Pattern.matches("\\w*", prenume))
                this.prenume = prenume;
            else throw new Exception();

        if (Pattern.matches("\\d{13}", CNP))
                this.CNP = CNP;
            else throw new Exception();

        if (Pattern.matches("\\d{10,13}", NrTel))
            this.NrTel = NrTel;
            else throw new Exception();

    }

    

    public String getCNP() {
        return CNP;
    }

    public String getNrTel() {
        return NrTel;
    }

    public String getNume() {
        return nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setCNP(String CNP) throws Exception {
        if (Pattern.matches("\\d{13}", CNP))
                this.CNP = CNP;
            else throw new Exception();
    }

    public void setNrTel(String NrTel) throws Exception {
        if (Pattern.matches("\\d{10,13}", NrTel))
            this.NrTel = NrTel;
            else throw new Exception();
    }

    public void setNume(String nume) throws Exception {
        if (Pattern.matches("\\w*", nume))
                this.nume = nume;
            else throw new Exception();
    }

    public void setPrenume(String prenume) throws Exception {
        if (Pattern.matches("\\w*", prenume))
                this.prenume = prenume;
            else throw new Exception();
    }

    @Override
    public String toString() {
        return nume + " " + prenume + " " + CNP + " " + getNrTel();
    }



}
