//Stana Iulian
//315CA Tema1
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <dirent.h>
#include <string.h>
#include <time.h>


//Structura director
typedef struct Director {
	char *nume_fisier, timp_afisare[30], timp_comparare[30];
	int lungime_fisier, uid, gid, size, type;
} director;


//Functie de verificare a parametrilor
//Usage: ./lf [-a|-R|-r] <-s|-t>
//Intoarce contorul(vectorul de aparitii)
//In cazul in care nu e conform cerintelor primul element devine 2
int *verif_par(int na, char **arg)
{
	int i, j, gasit;
	char *list_par[] = { "-a", "-R", "-r", "-s", "-t" };
	int  *cont      = calloc(6, sizeof(int));

	for (i = 1; i < na; i++) {
		gasit = 0;
		for (j = 0; j < 5; j++) {
//Verific daca parametrul 'i' se afla in lista de parametri
			if (strcmp(arg[i], list_par[j]) == 0) {
//In caz afirmativ cresc contorul si gasit devine 1;
				cont[j]++;
				gasit = 1;
//In cazul in care parametru este dublat iese din functie
				if (cont[j] > 1) {
					cont[0] = 2;
					return cont;
				}
			}
		}
//In cazul in care parametru nu exista in lista mea de parametri
//iese din functie
		if (gasit == 0) {
			cont[0] = 2;
			return cont;
		}
//In cazul in care programul primeste si parametru -s si parametru
//-t se iese din functie
		if (cont[3] == 1 && cont[4] == 1) {
			cont[0] = 2;
			return cont;
		}
	}
//In cazul in care verificarea a decurs bine se returneaza functia de contor
	return cont;
}


//Functia returneaza fisierele din direcorul curent
//valoarea care o primeste prin variabila "nr" se modifica la iesire
//parametri[] = { "-a", "-R", "-r", "-s", "-t" };
//parametri[] = { 0, 0, 0, 0, 0};
director *lista_fis(char *Direc_Name, int *parametri, int *nr)
{
//Initializez toate variabiilele pe care le folosesc
	struct dirent *entry;
	struct stat     info;
	struct tm     *timeinfo;
	char time_afis[30], time_comp[30], *path;
	DIR *dir;

	director *sis_fis;
//Aloc memorie structuri mele in care urmez sa memorez toate fisierele
//din directorul curent
	sis_fis = (director *) malloc((*nr) * sizeof(director));

	dir = opendir(Direc_Name);
	while ((entry = readdir(dir)) != NULL) {

		if (strcmp(entry->d_name, ".")  == 0)
			continue;
		if (strcmp(entry->d_name, "..") == 0)
			continue;
		if (strcmp(entry->d_name, "lf") == 0)
			continue;
		if (parametri[0] == 0 && entry->d_name[0] == '.')
			continue;
		else {
//Realoc memorie structurii mele pentru a putea baga un nou element in el
			sis_fis = (director *)
                      realloc(sis_fis,((*nr) + 1) * sizeof(director));
//Aloc memorie variabilei path pentru a putea lua calea absoluta a directorului
			path    = malloc((strlen(Direc_Name) + strlen(entry->d_name) + 2)
                     * sizeof(char));
			strcpy(path, Direc_Name);
			strcat(path, "/");
			strcat(path, entry->d_name);

//Apelez functia stat si iau toate informatiile fisierului path
			stat(path, &info);
			timeinfo = localtime(&info.st_mtime);
			strftime(time_afis, 30, "%H:%M:%S, %d %B %Y", timeinfo);
			strftime(time_comp, 30, "%Y%m%d%H%M%S", timeinfo);
//            free(timeinfo);

//Creez doua stringuri unul pentru afisarea timpului
//si al doulea pentru compararea timpului

			sis_fis[*nr].nume_fisier    = strdup(entry->d_name);
			sis_fis[*nr].lungime_fisier = strlen(entry->d_name);
			sis_fis[*nr].uid            = (int)info.st_uid;
			sis_fis[*nr].gid            = (int)info.st_gid;
			sis_fis[*nr].size           = (int)info.st_size;
			strcpy(sis_fis[*nr].timp_afisare,   time_afis);
			strcpy(sis_fis[*nr].timp_comparare, time_comp);

            sis_fis[*nr].type = entry->d_type;

//Cresc numarul de fisiere
			(*nr)++;
			free(path);

		}
	}
	closedir(dir);
    free(entry);
	return sis_fis;

}

//sortare crescatoare simpla
int sort_cres(director a, director b){

	return strcmp(a.nume_fisier, b.nume_fisier);
}

//sortare descrescatoare simpla
int sort_descres(director a, director b){

	return strcmp(b.nume_fisier, a.nume_fisier);
}

//sortare crescatoare supa size
int sort_Size(director a, director b){

	if (a.size - b.size == 0)
		return strcmp(a.nume_fisier, b.nume_fisier);
	else
		return a.size - b.size;
}

//sortare descrescatoare dupa size
int sort_SizeDe(director a, director b){

	if (b.size - a.size == 0)
		return strcmp(b.nume_fisier, a.nume_fisier);
	else
		return b.size - a.size;
}

//sortare crescatoare dupa timp
int sort_Timp(director a, director b){

	if (strcmp(a.timp_comparare, b.timp_comparare) == 0)
		return strcmp(a.nume_fisier, b.nume_fisier);
	else
		return strcmp(a.timp_comparare, b.timp_comparare);
}

//sortare descrescatoare dupa timp
int sort_TimpDe(director a, director b){

	if (strcmp(b.timp_comparare, a.timp_comparare) == 0)
		return strcmp(b.nume_fisier, a.nume_fisier);
	else
		return strcmp(b.timp_comparare, a.timp_comparare);
}

//Functia de partitie implementata la Quicksort
int partitie(int lower, int uper, director * sis_fis,
              int (*comparare) (director, director))
{
	director auxiliar, pivot = sis_fis[lower];	/* alegere pivot */
	int i = lower - 1;
	int j = uper + 1;
	while (1) {
		do {
			i++;
		} while (comparare(sis_fis[i], pivot) < 0);
		do {
			j--;
		} while (comparare(sis_fis[j], pivot) > 0);
		if (i < j) {
			auxiliar   = sis_fis[i];
			sis_fis[i] = sis_fis[j];
			sis_fis[j] = auxiliar;
		} else
			return j;
	}
}

//Implementarea functiei de sotare Quicksort
void QuickSort(int lower, int uper, director * sis_fis,
	       int (*comparare) (director, director))
{
	if (lower < uper) {
		int part = partitie(lower, uper, sis_fis, comparare);
		QuickSort(lower   , part, sis_fis, comparare);
		QuickSort(part + 1, uper, sis_fis, comparare);
	}
}


//Functia de sortare dupa parametri
//                  0     1     2     3     4
//parametri[] = { "-a", "-R", "-r", "-s", "-t" };
//parametri[] = { 0, 0, 0, 0, 0};
void sortare(director * sis_fis, int *parametri, int nr_fisiere)
{
//sortare crescator simplu
	if (parametri[2] == 0 && parametri[3] == 0 && parametri[4] == 0)
		QuickSort(0, nr_fisiere - 1, sis_fis, &sort_cres);
//sortare descresctor simplu
	else if (parametri[2] == 1 && parametri[3] == 0 && parametri[4] == 0)
		QuickSort(0, nr_fisiere - 1, sis_fis, &sort_descres);
//sortare dupa size crescator
	else if (parametri[2] == 0 && parametri[3] == 1 && parametri[4] == 0)
		QuickSort(0, nr_fisiere - 1, sis_fis, &sort_Size);
//sortare dupa size descrescator
	else if (parametri[2] == 1 && parametri[3] == 1 && parametri[4] == 0)
		QuickSort(0, nr_fisiere - 1, sis_fis, &sort_SizeDe);
//sortare dupa timp crescator
	else if (parametri[2] == 0 && parametri[3] == 0 && parametri[4] == 1)
		QuickSort(0, nr_fisiere - 1, sis_fis, &sort_Timp);
//sortare dupa timp descrecator
	else if (parametri[2] == 1 && parametri[3] == 0 && parametri[4] == 1)
		QuickSort(0, nr_fisiere - 1, sis_fis, &sort_TimpDe);
}

//Afisez in formatul dorit
//list.c        1000:1000       1705    23:47:26, 09 March 2011
//Functia primeste parametru indent pentru a putea alinia la dreapta numele fisierului
void print_afisare(director afisare, int indent)
{
//%*s imi aliniaza in functie de indent la dreapta numele fisierului
	printf("%*s\t%d:%d\t%d\t%s\n", indent, afisare.nume_fisier,
	       afisare.uid, afisare.gid, afisare.size, afisare.timp_afisare);

}


//Functia de determinare a fisierului cu lungimea cea mai mare
int aranjez_dreapta(director * sis_fis, int nr_fisiere)
{
	int i, max = sis_fis[0].lungime_fisier;
	for (i = 1; i < nr_fisiere; i++)
		if (sis_fis[i].lungime_fisier > max)
			max = sis_fis[i].lungime_fisier;

	return max;
}


//Functia recursiva pentru afisarea fisierelor
//parametri[] = { "-a", "-R", "-r", "-s", "-t" };
//parametri[] = { 0, 0, 0, 0, 0};
void rec_fis(char *Direc_Name, int *parametri, int indent)
{
//Declararea variabilelor
	int nr_fisiere = 0, lung_max = 0, count, i;
	director *sis_fis;
	char *path;

//Memorarea fisierelor in structura sis_fis
	sis_fis = lista_fis(Direc_Name, parametri, &nr_fisiere);

//Sortarea dupa criteriile alese
	sortare(sis_fis, parametri, nr_fisiere);

//Determinarea fisierului cu lungimea cea mai mare
	if (nr_fisiere > 1)
		lung_max = aranjez_dreapta(sis_fis, nr_fisiere);

//Parcurgerea structurii
	for (i = 0; i < nr_fisiere; i++) {
//Indetarea cu taburi dupa caz
		for (count = 0; count < indent; count++)
                printf("\t");
//Afisarea fisierului in formatul dorit
		print_afisare(sis_fis[i], lung_max);

//In cazul in care programul primeste parametru '-R' se apeleaza recursiv
//functia rec_fis in directoarele structuri 'sis_fis'
		if (sis_fis[i].type == 4 && parametri[1] == 1) {
			path = malloc((strlen(Direc_Name) +
                  strlen(sis_fis[i].nume_fisier) + 3) * sizeof(char));
			strcpy(path, Direc_Name);
			strcat(path, "/");
			strcat(path, sis_fis[i].nume_fisier);
			rec_fis(path, parametri, indent + 1);

            free(path);
		}
        free(sis_fis[i].nume_fisier);
	}
	free(sis_fis);

}

int main(int na, char **arg)
{

	int *parametri;
//Verificarea corectitudinii paramaterilor introdusi
	parametri = verif_par(na, arg);
	if (parametri[0] == 2) {
		printf("Usage: ./lf [-a|-R|-r] <-s|-t>\n");
        free(parametri);
		return 0;
	}

//Aflarea directorului curent si apelarea functiei rec_fis
    char *InitialDirectory = malloc (100*sizeof(char));
	getcwd(InitialDirectory, 100);
	rec_fis(InitialDirectory, parametri, 0);

    free(InitialDirectory);
	free(parametri);
	return 0;

}
